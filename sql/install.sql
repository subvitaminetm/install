--
-- Création des tables
--

CREATE TABLE base_config (	
	id int(4) AUTO_INCREMENT,
	`key` varchar(100) NOT NULL,
	value blob DEFAULT NULL,
	date_created datetime DEFAULT CURRENT_TIMESTAMP,
	date_updated datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	INDEX (`key`),
	UNIQUE (`key`)
)Engine=InnoDB;

CREATE TABLE base_language (
        id int(4) AUTO_INCREMENT,
        name varchar(255) NOT NULL,
        code varchar(3) NOT NULL,
        real_code varchar(10) NOT NULL,
        date_created datetime DEFAULT CURRENT_TIMESTAMP,
	date_updated datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	INDEX (`code`),
	UNIQUE (`code`)
)Engine=InnoDB;

CREATE TABLE base_role (
	id int(4) AUTO_INCREMENT,
	`key` varchar(50) NOT NULL,
	name varchar(100) NOT NULL,
	status tinyint(1) DEFAULT 1,
	date_created datetime DEFAULT CURRENT_TIMESTAMP,
	date_updated datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	INDEX (`key`),
	UNIQUE (`key`)
)Engine=InnoDB;

CREATE TABLE base_user (
	id int(4) AUTO_INCREMENT,
	uniqid varchar(50) NOT NULL,
	email varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	firstname varchar(100) NOT NULL,
	lastname varchar(100) NOT NULL,
	status int(4) NOT NULL DEFAULT 2,
	id_language int(4) NOT NULL,
	id_role int(4) NOT NULL,
	date_created datetime DEFAULT CURRENT_TIMESTAMP,
	date_updated datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	FOREIGN KEY (id_language) REFERENCES base_language (id),
	FOREIGN KEY (id_role) REFERENCES base_role (id),
	INDEX (`email`),
	UNIQUE (uniqid),
	UNIQUE (email)
)Engine=InnoDB;

CREATE TABLE base_token (
	id int(4) AUTO_INCREMENT,
	`key` varchar(50) NOT NULL,
	date_expire datetime NOT NULL,
	id_user int(4) NOT NULL,
	date_created datetime DEFAULT CURRENT_TIMESTAMP,
	date_updated datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	FOREIGN KEY (id_user) REFERENCES base_user (id),
	INDEX (`key`),
	UNIQUE (`key`)
)Engine=InnoDB;

--
-- Insertion des datas
--

INSERT INTO `base_role` (`id`, `key`, `name`, `status`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 1),
(2, 'ADMIN', 'Administrateur', 1),
(3, 'USER', 'Utilisateur', 1);

INSERT INTO `base_language` (`id`, `name`, `code`, `real_code`) VALUES
(1, 'Français', 'fr', 'fr_FR');

INSERT INTO `base_user` (`id`, `uniqid`, `email`, `password`, `firstname`, `lastname`, `status`, `id_language`, `id_role`) VALUES
(1, '69bb4cde20502e3e50212b23af83c780', 'mehdi@busineyss.com', '$2y$11$YZN4wTsRYsmn07Tk3ymc..00rt/T3MUHPAbJJBh3/Fv95M3bRRxNa','Mehdi', 'Salmi', 1, 1, 1),
(2, '69bb4cde20502e3e50212b23af83c781', 'julien@subvitamine.com', '$2y$11$5rg0Q87zuX.USALE77AyDuEVTYRfMGHUO7qeu1rIqnSKuIga2g7D2', 'Julien', 'Herrera', 1, 1, 1),
(3, '69bb4cde20502e3e50212b23af83c782', 'olacombe@subvitamine.com', '$2y$11$tHjBxeNkPpcLsu6SBEBstuvdNuP5.1sXupRekUrxRW1dXCVanSLQK', 'Olivier', 'Lacombe', 1, 1, 1);