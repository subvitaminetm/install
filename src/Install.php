<?php

namespace Subvitamine;

class Install {

    protected $_basePath;
    protected $_steps = array(
        'database' => 'routes',
        'routes' => 'end',
        'end' => ''
    );
    
    public function __construct() {
        $this->_basePath = realpath(__DIR__ . '/') . '/../templates';
    }

    public function start() {

        $step = ((!empty($_GET['step']) && array_key_exists($_GET['step'], $this->_steps)) || $_GET['step'] == "redirect") ? $_GET['step'] : 'database';
        
        if($step == 'redirect' && file_exists(CONFIGS_PATH . DS . 'database.ini') && file_exists(CONFIGS_PATH . DS . 'routes.ini')){
            $routes = new \Zend_Config_Ini(CONFIGS_PATH . DS . 'routes.ini', APPLICATION_ENV);
            $synapseRoute = str_replace(':module', 'synapse', $routes->resources->router->routes->www->route);
            header('Location://' . $synapseRoute);
            exit;
        }

        // set view
        $view = new \Zend_View();
        $view->setBasePath($this->_basePath);
        $view->message = null;
        $view->template = $step;
        $view->params = $params;

        // set post action
        if (!empty($_POST)) {
            $params = (object) $_POST;
            $view->params = $params;
            $functionName = '_' . $step;
            $response = $this->{$functionName}($params);
            if ($response->status && !empty($this->_steps[$step])) {
                header('Location:' . $_SERVER['HTTP_ORIGIN'] . '/?step=' . $this->_steps[$step]);
                exit;
            } else {
                $view->message = $response->message;
            }
        }

        // render view
        echo $view->render('index.phtml');
        exit;
    }

    private function _database($params) {

        $response = new \stdClass();

        /**
         * CREATE DATABASE
         */
        $conn = new \mysqli($params->host, $params->username, $params->password);
        if ($conn->connect_error) {
            $response->status = false;
            $response->message = "<strong>Connection failed :</strong> " . $conn->connect_error;
            return $response;
        }
        $sql = "CREATE DATABASE `" . $params->dbname . "` CHARACTER SET utf8 COLLATE utf8_general_ci";
        if ($conn->query($sql) !== TRUE) {
            $response->status = false;
            $response->message = "<strong>Une erreur est survenue durant la création de la base de donnée !</strong><br /><br />";
            $response->message .= "<strong>Exception :</strong> " . $conn->error . "<br /><br />";
            $response->message .= "<strong>SQL :</strong> " . $sql;
            return $response;
        }
        $conn->close();

        /**
         * CREATE TABLES & INSERT DATA
         */
        $db = new \mysqli($params->host, $params->username, $params->password, $params->dbname);
        $db->set_charset("utf8");
        $tablesAndDataSql = file_get_contents(realpath(__DIR__ . '/') . '/../sql/install.sql');
        try {
            mysqli_multi_query($db, $tablesAndDataSql);
            $db->close();
        } catch (Exception $ex) {
            $db->close();
            $response->status = false;
            $response->message = "<strong>Une erreur est survenue durant la création des tables dans la base de donnée !</strong><br /><br />";
            $response->message .= "<strong>Exception :</strong> " . $ex->getMessage() . "<br /><br />";
            return $response;
        }


        /**
         * CREATE DATABASE.INI
         */
        $data = array('production' => array(
                'multidbname' => $params->multidbname,
                'resources.multidb.' . $params->multidbname . '.adapter' => 'Pdo_Mysql',
                'resources.multidb.' . $params->multidbname . '.default' => 1,
                'resources.multidb.' . $params->multidbname . '.charset' => 'utf8',
                'resources.multidb.' . $params->multidbname . '.host' => $params->host,
                'resources.multidb.' . $params->multidbname . '.dbname' => $params->dbname,
                'resources.multidb.' . $params->multidbname . '.username' => $params->username,
                'resources.multidb.' . $params->multidbname . '.password' => $params->password
        ));
        $data['development : staging'] = $data['staging : production'] = $data['production'];
        file_put_contents(CONFIGS_PATH . 'database.ini', $this->_arr2ini($data));
        $response->status = true;
        return $response;
    }

    private function _routes($params) {
        $response = new \stdClass();
        $data = array('production' => array(
                'routes.www.type' => 'Zend_Controller_Router_Route_Hostname',
                'routes.www.route' => ':module.' . $params->domain,
                'routes.www.defaults.module' => 'www',
                'routes.www.chains.default.type' => 'Zend_Controller_Router_Route',
                'routes.www.chains.default.route' => ':lang/:@controller/:@action/*',
                'routes.www.chains.default.defaults.lang' => 'en',
                'routes.www.chains.default.defaults.controller' => 'index',
                'routes.www.chains.default.defaults.action' => 'index'
        ));
        $data['development : staging'] = $data['staging : production'] = array('routes.www.route' => ':module.' . $params->domain);
        
        file_put_contents(CONFIGS_PATH . 'routes.ini', $this->_arr2ini($data));
        $response->status = true;
        return $response;
    }
    
    private function _arr2ini(array $a, array $parent = array()) {
        $out = '';
        foreach ($a as $k => $v) {
            if (is_array($v)) {
                //subsection case
                //merge all the sections into one array...
                $sec = array_merge((array) $parent, (array) $k);
                //add section information to the output
                $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
                //recursively traverse deeper
                $out .= $this->_arr2ini($v, $sec);
            } else {
                //plain key->value case
                if (preg_match('/\[\]$/', $k)) {
                    $values = explode(',', $v);
                    $values = array_map('trim', $values);
                    foreach ($values as $val) {
                        $out .= $this->_arr2iniOutput(array('key' => $k, 'value' => $val));
                    }
                } else {
                    $out .= $this->_arr2iniOutput(array('key' => $k, 'value' => $v));
                }
            }
        }
        return $out;
    }

    public function _arr2iniOutput($arr) {
        if (is_int($arr['value']) || preg_match('/APPLICATION_PATH/', $arr['value'])) {
            return $arr['key'] . '=' . $arr['value'] . PHP_EOL;
        } else {
            return $arr['key'] . '="' . $arr['value'] . '"' . PHP_EOL;
        }
    }

}
